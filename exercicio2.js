const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});


// GET CLIENTE
app.get('/clientes',(req,res)=>{
    let source = req.query;
    let msg = `Dados do Cliente: ${source.nome} ${source.sobrenome}`
    res.send(`{message: ${msg}}`);
});

// POST CLIENTE
app.post('/clientes/:valor',(req,res)=>{
    let dados = req.body.valor;
    let headers_ = req.headers["access"];
    let ret = `\nDados enviados: ${dados} 
                \nCliente \nNome: ${dados.nome} \nSobrenome: ${dados.sobrenome}`;
    let msg = '';

    if(headers_ == 98765){
        msg = 'Acesso Permitido!\n';
        res.send(`\n{message: ${msg} ${ret}}`);
    }else{
        msg = 'Acesso Negado!';
        res.send(`\n{message: ${msg}}`);
    }
})




// GET FUNCIONARIO
app.get('/funcionarios',(req,res)=>{
    let source = req.query;
    let msg = `Dados do Funcionário: ${source.nome} ${source.sobrenome}`
    res.send(`{message: ${msg}}`);
});



//DELETE FUNCIONARIO
app.delete('/funcionarios/:valor',(req,res)=>{
    let dados = req.query;
    let valor1 = req.params.valor;
    let headers_ = req.headers["access"];
    let msg = '';

    if(headers_ == 12345){
        msg = 'Acesso Permitido!\n';
        res.send(`${msg} \n{message: Dados enviados: ${valor1}}
                    \nNome: ${dados.nome}
                    Sobrenome: ${dados.sobrenome}
                    Idade: ${dados.idade}`);
    }else{
        msg = 'Acesso Negado!';
        res.send(`\n{message: ${msg}}`);
    }

})


//PUT FUNCIONARIO
app.put('/funcionarios/:valor',(req,res)=>{
    let dados = req.body;
    let valor1 = req.body.valor;
    let headers_ = req.headers["access"];
    let msg = '';

    if(headers_ == 54321){
        msg = 'Acesso Permitido!\n';
        res.send(`${msg} \n{message: Dados enviados: ${valor1}}
                    \nNome: ${dados.nome}
                    Sobrenome: ${dados.sobrenome}
                    Idade: ${dados.idade}`);
    }else{
        msg = 'Acesso Negado!';
        res.send(`\n{message: ${msg}}`);
    }
})

//recurso de request.param
app.get('/aluno/pesquisa/:valor',(req, res)=>{
    console.log("Entrou");
    let dado = req.params.valor;
    let ret = "Dados solicitado:" + dado;
    res.send("{message:"+ret+"}");
});
